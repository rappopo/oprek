const helper = require('../../lib/helper')
const compressing = require('compressing')
const { DateTime } = require('luxon')
const util = require('util')
const fs = require('fs-extra')
const fstat = util.promisify(fs.stat)

module.exports = function (file, format, retainSource) {
  const start = DateTime.local()
  const result = {}
  const dest = `${file}.${format}`
  return new Promise((resolve, reject) => {
    if (!fs.existsSync) return reject(new Error(`File not found: '${file}`))
    fstat(file)
      .then(stat => {
        result.sizeBefore = stat.size
        return compressing[format === 'gz' ? 'gzip' : format].compressFile(file, dest)
      })
      .then(() => {
        return fstat(dest)
      })
      .then(stat => {
        result.sizeAfter = stat.size
        result.duration = helper.age(start)
        if (!retainSource) return fs.unlink(file)
        return ''
      })
      .then(() => resolve(result))
      .catch(reject)
  })
}
