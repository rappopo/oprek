const helper = require('../../lib/helper')
const compressing = require('compressing')
const path = require('path')
const { DateTime } = require('luxon')
const util = require('util')
const fs = require('fs-extra')
const fstat = util.promisify(fs.stat)

module.exports = function (file, retainSource) {
  const start = DateTime.local()
  const result = {}
  let dest = path.dirname(file)
  const format = path.extname(file).substr(1).toLowerCase()
  if (format === 'gz') {
    dest = file.substr(0, dest.length - 3)
  }
  return new Promise((resolve, reject) => {
    if (!fs.existsSync) return reject(new Error(`File not found: '${file}`))
    fstat(file)
      .then(stat => {
        result.sizeBefore = stat.size
        return compressing[format === 'gz' ? 'gzip' : format].uncompress(file, dest)
      })
      .then(() => {
        return fstat(dest)
      })
      .then(stat => {
        result.sizeAfter = stat.size
        result.duration = helper.age(start)
        if (!retainSource) return fs.unlink(file)
        return ''
      })
      .then(() => resolve(result))
      .catch(reject)
  })
}
