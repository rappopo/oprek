const fs = require('fs-extra')
const JSONStream = require('JSONStream')
const { DateTime } = require('luxon')
const _ = require('lodash')
const helper = require('../../lib/helper')
const path = require('path')
const { DataStream } = require('scramjet')
const countLines = require('../count-lines')

module.exports = function ({ source, dest, modifier, ensureDir, fields, excludeFields, progress }) {
  const start = DateTime.local()
  if (ensureDir) fs.ensureDirSync(path.dirname(dest))
  let rows = 0
  const bar = progress ? helper.initProgressBar() : null
  if (fields) fields = fields.replace(/\s+/g, ',').split(',')
  if (excludeFields) excludeFields = excludeFields.replace(/\s+/g, ',').split(',')
  return new Promise((resolve, reject) => {
    Promise.resolve()
      .then(() => {
        if (!bar) return ''
        return countLines(source, '},')
      })
      .then(lines => {
        if (bar) bar.start(lines, 0)
        const writer = fs.createWriteStream(dest)
        writer.on('error', err => {
          if (bar) bar.stop()
          reject(err)
        })
        writer.on('finish', () => {
          if (bar) {
            bar.setTotal(rows)
            bar.stop()
          }
          resolve({ rows, duration: helper.age(start) })
        })
        DataStream
          .pipeline(
            fs.createReadStream(source),
            JSONStream.parse('*')
          )
          .map(async item => {
            rows++
            if (bar) bar.increment()
            if (modifier) item = await modifier(item, helper)
            if (fields) item = _.pick(item, fields)
            if (excludeFields) item = _.omit(item, excludeFields)
            return JSON.stringify(item) + '\n'
          })
          .pipe(writer)
      })
  })
}
