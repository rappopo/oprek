const fs = require('fs-extra')
const { DateTime } = require('luxon')
const _ = require('lodash')
const helper = require('../../lib/helper')
const path = require('path')
const { StringStream } = require('scramjet')
const countLines = require('../count-lines')

module.exports = function ({ source, dest, modifier, ensureDir, fields, excludeFields, progress }) {
  const start = DateTime.local()
  if (ensureDir) fs.ensureDirSync(path.dirname(dest))
  let rows = 0
  const bar = progress ? helper.initProgressBar() : null
  if (fields) fields = fields.replace(/\s+/g, ',').split(',')
  if (excludeFields) excludeFields = excludeFields.replace(/\s+/g, ',').split(',')
  return new Promise((resolve, reject) => {
    Promise.resolve()
      .then(() => {
        if (!bar) return ''
        return countLines(source)
      })
      .then(lines => {
        if (bar) bar.start(lines, 0)
        const reader = fs.createReadStream(source)
        const writer = fs.createWriteStream(dest)
        const onError = err => {
          if (bar) bar.stop()
          reject(err)
        }
        reader.on('error', onError)
        writer.on('error', onError)
        writer.on('finish', () => {
          if (bar) {
            bar.setTotal(rows)
            bar.stop()
          }
          fs.appendFileSync(dest, ']')
          resolve({ rows, duration: helper.age(start) })
        })
        let first = true
        writer.write('[')
        StringStream
          .from(reader)
          .lines()
          .map(async item => {
            item = _.trim(item)
            if (_.isEmpty(item)) return
            rows++
            if (bar) bar.increment()
            try {
              item = JSON.parse(item)
              if (modifier) item = await modifier(item, helper)
              if (fields) item = _.pick(item, fields)
              if (excludeFields) item = _.omit(item, excludeFields)
              item = JSON.stringify(item)
              if (!first) item = ',' + item
              first = false
              return item
            } catch (err) {}
          })
          .pipe(writer)
      })
  })
}
