/**
 * Based on https://stackoverflow.com/questions/12453057/node-js-count-the-number-of-lines-in-a-file
 * @param {*} file - File name
 */

const fs = require('fs')

module.exports = function (file, token = 10) {
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(file)) return reject(new Error('File not found'))
    let lineCount = 0
    fs.createReadStream(file)
      .on('data', (buffer) => {
        let idx = -1
        lineCount--
        do {
          idx = buffer.indexOf(token, idx + 1)
          lineCount++
        } while (idx !== -1)
      }).on('end', () => {
        resolve(lineCount)
      }).on('error', reject)
  })
}
