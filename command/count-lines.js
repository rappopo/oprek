const path = require('path')
const helper = require('../lib/helper')
const command = 'countLines <file>'
const desc = `Count how many lines are in a file`

const handler = function (argv) {
  require('../handler/count-lines')(path.resolve(argv.file))
    .then(lines => helper.logger.info(`Total: ${lines} lines`))
    .catch(helper.exit)
}

module.exports = { command, desc, handler }
