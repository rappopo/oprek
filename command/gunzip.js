const command = 'gunzip <file>'
const path = require('path')
const helper = require('../lib/helper')
const desc = `GUnzip a file. Alias for 'oprek uncompressFile' for gzip files only`

const builder = {
  deleteSource: {
    alias: 'd',
    desc: `Delete source file`,
    default: false
  }
}

const handler = function (argv) {
  const ext = path.extname(argv.file).substr(1).toLowerCase()
  if (ext !== 'gz') return helper.exit(new Error('Unsupported format'))
  require('./uncompress-file').handler(argv)
}

module.exports = { command, desc, builder, handler }
