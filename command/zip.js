const command = 'zip <file>'
const desc = `Zip a file. Alias for 'oprek compressFile --format=zip'`

const builder = {
  deleteSource: {
    alias: 'd',
    desc: `Delete source file`,
    default: false
  }
}

const handler = function (argv) {
  argv.format = 'zip'
  require('./compress-file').handler(argv)
}

module.exports = { command, desc, builder, handler }
