const path = require('path')
const helper = require('../lib/helper')
const { _, numbro } = helper
const command = 'compress <file>'
const supported = ['zip', 'gz']
const desc = `Compress a file. Supported format: ${supported.join(', ')}. Defaults to zip`

const builder = {
  format: {
    alias: 'f',
    default: 'zip',
    desc: `Compression format (${supported.join(', ')})`
  },
  deleteSource: {
    alias: 'd',
    desc: `Delete source file`,
    default: false
  }
}

const handler = function (argv) {
  let format = 'zip'
  if (_.isString(argv.format) && supported.includes(argv.format)) format = argv.format
  else helper.exit(new Error('Unsupported format'))
  require('../handler/misc/compress')(path.resolve(argv.file), format, !argv.deleteSource)
    .then(({ sizeBefore, sizeAfter, duration }) => {
      const b = numbro(sizeBefore).format({ output: 'byte', base: 'binary', spaceSeparated: true, mantissa: 1 })
      const a = numbro(sizeAfter).format({ output: 'byte', base: 'binary', spaceSeparated: true, mantissa: 1 })
      const p = numbro(sizeAfter / sizeBefore).format({ output: 'percent', mantissa: 1 })
      helper.logger.info(`Before: ${b}, After: ${a}, Rate: ${p}, Duration: ${duration}`)
    })
    .catch(helper.exit)
}

module.exports = { command, desc, builder, handler }
