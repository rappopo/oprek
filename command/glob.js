const _ = require('lodash')
const path = require('path')
const fs = require('fs-extra')
const fg = require('fast-glob')
const helper = require('../lib/helper')
const { each } = require('lodash')
const command = 'glob <pattern>'
const desc = 'list directory/files with glob, and save the output as json file or console. Use options for quick record modifier tuneup or use dedicated modifier file'

const builder = {
  destination: {
    alias: 'd',
    desc: 'Destination file to save. Displayed on screen if omitted'
  },
  modifier: {
    alias: 'm',
    desc: 'Modifier file. If used, all other options (except dest file) are ignored'
  },
  fileField: {
    alias: 'f',
    desc: 'File field',
    default: 'file'
  },
  nameField: {
    alias: 'n',
    desc: 'Generated name field',
    default: 'name'
  },
  nameGenerator: {
    alias: 'g',
    desc: 'Name field generator with lodash string functions',
    default: 'startCase'
  },
  baseDir: {
    desc: 'Base/start dir. Defaults: current dir'
  },
  onlyDir: {
    desc: 'List directories only'
  },
  dot: {
    desc: 'Allow dot files'
  }
}

const handler = function (argv) {
  const opts = {}
  if (argv.baseDir) opts.cwd = argv.baseDir
  if (argv.onlyDir) opts.onlyDirectories = true
  if (argv.dot) opts.dot = true
  const files = fg.sync(argv.pattern, opts)
  let modifier
  if (argv.modifier) {
    const mfile = path.resolve(argv.modifier)
    console.log(mfile)
    if (!fs.existsSync(mfile)) return helper.exit(new Error('Modifier file not found'))
    modifier = require(mfile)
  }
  if (!_[argv.nameGenerator]) return helper.exit(new Error('Invalid name generator'))
  if (files.length === 0) return helper.exit(new Error('Glob results nothing. Perhaps you might need to check your pattern'))
  const result = []
  each(files, f => {
    let rec = {}
    if (modifier) {
      rec = modifier(f, { _ })
    } else {
      rec[argv.fileField] = f
      rec[argv.nameField] = _[argv.nameGenerator](f)
    }
    result.push(rec)
  })
  if (!argv.destination) return console.log(JSON.stringify(result, null, 2))
  fs.outputJsonSync(argv.destination, result, { spaces: 2 })
  helper.logger.info(`Result saved as ${argv.destination}`)
}

module.exports = { command, desc, builder, handler }
