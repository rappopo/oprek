const _ = require('lodash')
const helper = require('../lib/helper')
const geo = require('kea-geo')
const command = 'geo <fn>'
const desc = 'Calculate kea-geo functions'

const builder = {
}

const handler = function (argv) {
  if (!_.keys(geo).includes(argv.fn)) return helper.exit(new Error(`No such function found`))
  argv._.shift()
  const params = argv._
  _.each(params, (p, i) => {
    try {
      params[i] = JSON.parse(p)
    } catch (err) {
      return helper.exit(new Error(`Can't parse this: '${p}'`))
    }
  })
  const result = geo[argv.fn].apply(null, params)
  helper.logger.info(`Params: ${JSON.stringify(params)}`)
  helper.logger.info(`Return: ${JSON.stringify(result)}`)
}

module.exports = { command, desc, builder, handler }
