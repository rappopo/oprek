const command = 'gzip <file>'
const desc = `GZip a file. Alias for 'oprek compressFile --format=gz'`

const builder = {
  deleteSource: {
    alias: 'd',
    desc: `Delete source file`,
    default: false
  }
}

const handler = function (argv) {
  argv.format = 'gz'
  require('./compress-file').handler(argv)
}

module.exports = { command, desc, builder, handler }
