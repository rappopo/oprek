const path = require('path')
const helper = require('../lib/helper')
const { numbro } = helper
const command = 'uncompress <file>'
const supported = ['zip', 'gz']
const desc = `Uncompress a file. Supported format: ${supported.join(', ')}`

const builder = {
  deleteSource: {
    alias: 'd',
    desc: `Delete source file`,
    default: false
  }
}

const handler = function (argv) {
  const ext = path.extname(argv.file).substr(1).toLowerCase()
  if (!supported.includes(ext)) return helper.exit(new Error('Unsupported format'))
  require('../handler/misc/uncompress')(path.resolve(argv.file), !argv.deleteSource)
    .then(({ sizeBefore, sizeAfter, duration }) => {
      const b = numbro(sizeBefore).format({ output: 'byte', base: 'binary', spaceSeparated: true, mantissa: 1 })
      const a = numbro(sizeAfter).format({ output: 'byte', base: 'binary', spaceSeparated: true, mantissa: 1 })
      const p = numbro(sizeAfter / sizeBefore).format({ output: 'percent', mantissa: 1 })
      helper.logger.info(`Before: ${b}, After: ${a}, Rate: ${p}, Duration: ${duration}`)
    })
    .catch(helper.exit)
}

module.exports = { command, desc, builder, handler }
