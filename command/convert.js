const helper = require('../lib/helper')
const _ = require('lodash')
const fs = require('fs-extra')
const path = require('path')
const supportedFormat = ['json', 'jsonl', 'csv']
const command = 'convert <source> <format/destination>'
const desc = `Convert a single file to another format. File format will automatically detected based on its file extension. Supported format: ${supportedFormat.join(', ')}`
const doCompress = require('../handler/misc/compress')

const builder = {
  ensureDir: {
    alias: 'e',
    desc: 'If destination dir is set, ensure its existent'
  },
  compress: {
    alias: 'c',
    desc: 'Compress dest file. Supported: zip & gz. Default: zip'
  },
  fields: {
    alias: 'f',
    desc: 'Only use this fields (comma separated)'
  },
  excludeFields: {
    alias: 'x',
    desc: 'Exclude this fields (comma separated)'
  },
  modifier: {
    alias: 'm',
    desc: 'A node module modifier file'
  }
}

const handler = function (argv) {
  let format = argv['format/destination']
  if (format.substr(0, 1) === '.') format = format.substr(1)
  if (!fs.existsSync(argv.source)) helper.exit(new Error('File not found'))
  argv.source = path.resolve(argv.source)
  const sourceDir = path.dirname(argv.source)
  const sourceExt = path.extname(argv.source)
  const sourceBase = path.basename(argv.source, sourceExt)
  const sourceFormat = sourceExt.toLowerCase().substr(1)
  if (!supportedFormat.includes(sourceFormat)) helper.exit(new Error('Unsupported source format'))
  let dest = format.split('.')
  const destExt = '.' + (dest.length === 1 ? dest[0] : _.last(dest))
  const destFormat = destExt.toLowerCase().substr(1)
  if (!supportedFormat.includes(destFormat)) helper.exit(new Error('Unsupported destination format'))
  let destBase = sourceBase
  let destDir = sourceDir
  if (dest.length > 1) {
    if (argv['format/destination'].substr(0, 1) === '.') dest = path.resolve(argv['format/destination'])
    else dest = argv['format/destination']
    destDir = path.dirname(dest)
    destBase = path.basename(dest, destExt)
  }
  destDir = path.resolve(destDir)
  if (sourceFormat === destFormat) helper.exit(new Error('Destination format must be different than its source'))
  if (!argv.ensureDir && !fs.existsSync(destDir)) helper.exit(new Error('Destination directory doesn\'t exist'))
  dest = path.join(destDir, destBase + destExt)
  let compress = ''
  if (argv.compress) {
    if (_.isBoolean(argv.compress)) compress = 'zip'
    else compress = argv.compress
    if (!['zip', 'gz'].includes(compress)) helper.exit(new Error('Unsupported compression format'))
  }
  let modifier
  if (argv.modifier) {
    if (_.isBoolean(argv.modifier)) helper.exit(new Error('Modifier has to be a nodejs module file'))
    argv.modifier = path.resolve(argv.modifier)
    if (!fs.existsSync(argv.modifier)) helper.exit(new Error('Modifier file not found'))
    modifier = require(argv.modifier)
  }
  helper.logger.info(`Source: ${argv.source}`)
  helper.logger.info(`Destination: ${dest}${compress !== '' ? ('.' + compress) : ''}`)
  if (argv.modifier) helper.logger.info(`Modifier: ${argv.modifier}`)
  const processor = `../handler/converter/${sourceFormat}2${destFormat}`
  let resp = {}
  require(processor)(_.merge(argv, { dest, modifier, progress: true }))
    .then(result => {
      resp = result
      if (!argv.compress) return ''
      return doCompress(dest, compress)
    })
    .then(result => {
      helper.logger.info(`Total records: ${resp.rows}, duration: ${resp.duration}`)
      if (result !== '') helper.logger.info(`Compressed in ${resp.duration}`)
    })
    .catch(helper.exit)
}

module.exports = { command, desc, handler, builder }
