const { DateTime } = require('luxon')
const kleur = require('kleur')
const numbro = require('numbro')
const _ = require('lodash')
const cliProgress = require('cli-progress')
const appTitle = '[oprek]'

const age = (dt1, dt2, as = 'ms') => {
  if (!dt1) dt1 = DateTime.local()
  if (!dt2) dt2 = DateTime.local()
  const diff = Math.abs(dt1.diff(dt2).as('seconds'))
  const obj = secondsToTime(diff)
  _.forOwn(obj, (v, k) => {
    obj[k] = _.padStart(v, 2, '0')
  })
  let out = diff
  switch (as) {
    case 'hms': out = `${obj.h}:${obj.m}:${obj.s}`; break
    case 'ms': out = `${obj.m}:${obj.s}`; break
  }
  return out
}

const secondsToTime = secs => {
  var hours = Math.floor(secs / (60 * 60))
  var divMin = secs % (60 * 60)
  var minutes = Math.floor(divMin / 60)

  var divSec = divMin % 60
  var seconds = Math.ceil(divSec)

  return {
    h: hours,
    m: minutes,
    s: seconds
  }
}

const logger = {
  info (message) {
    console.log(kleur.grey(appTitle), kleur.green().bold(message))
  },
  error (err) {
    if (err instanceof Error) console.error(kleur.grey(appTitle), kleur.red().bold(err.message), err)
    else console.error(kleur.grey(appTitle), kleur.red().bold(err))
  }
}

const exit = item => {
  if (item instanceof Error) {
    logger.error(item.message)
    process.exit(1)
  }
  logger.info(item)
  process.exit(0)
}

const initProgressBar = (options = {}) => {
  const bar = new cliProgress.SingleBar({
    format: `${kleur.grey(appTitle)} ${kleur.yellow().bold('{bar} {percentage}% | {value}/{total}')} ${kleur.green().bold(options.labelChunks || 'Records')}`,
    barCompleteChar: '\u2588',
    barIncompleteChar: '\u2591',
    hideCursor: true
  })
  return bar
}

module.exports = { _, numbro, DateTime, logger, appTitle, age, exit, initProgressBar }
