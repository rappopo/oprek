# oprek

Your swiss army knife for your daily coding

## Install

```
npm install -g oprek
```

## Usage

```
oprek --help
```

## Features

As of v0.0.3, currently it can:

- Convert file from one format to another
- Count the number of lines of a file

## License

[MIT](LICENSE.md)
